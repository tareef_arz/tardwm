/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 5;        /* border pixel of windows */
static const unsigned int gappx     = 5;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;   /* 0: systray in the right corner, >0: systray on left of status text */ static const unsigned int systrayspacing = 2;   /* systray spacing */ static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/ static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
#define ICONSIZE 16
#define ICONSPACING 8
static const int user_bh            = 25;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "JetBrainsMono Nerd Font:size=10", "Noto Sans Arabic Condensed:style=Medium", "JoyPixels:size=10" };
static const char col_gray1[]       = "#222222"; // Bar Background Color 
static const char col_gray2[]       = "#333333"; // Unselected Client Border Color
static const char col_gray3[]       = "#bbbbbb"; // Normal Font Color
static const char col_gray4[]       = "#eeeeee"; // Selected Font Color + Title Bar Font Color 
static const char col_gray5[]       = "#2b2b2b"; // Selected Tag Background + Title Bar Backround 
static const char col_cyan[]        = "#AAAAAA"; // Selected Border
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_gray5,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "α", "β", "ت", "Δ", "V" };

static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 1;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
        /* xprop(1):
         *      WM_CLASS(STRING) = instance, class
         *      WM_NAME(STRING) = title
         */
        /* class      instance    title       tags mask     iscentered   isfloating   monitor */
        { "Gimp",     NULL,       NULL,       0,            0,           1,           -1 },
        { "Firefox",  NULL,       NULL,       1 << 8,       0,           0,           -1 },
        { "st-256color",  NULL,       NULL,       0,       1,           0,           -1 },
        { "Brave-browser",  NULL,       NULL,       0,       1,           0,           -1},
        { "Nm-applet",  NULL,       NULL,       0,       1,           0,           -1 },
        { "Gcolor3",  NULL,       NULL,       0,       1,           1,           -1 },
};


/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "󰕲",      tile },    /* first entry is default */
	{ "󰘔",      NULL },    /* no layout function means floating behavior */
	{ "󰕯",      monocle },
 	{ "@",      spiral },
 	{ "󰕴",      dwindle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenucmd[] = { "dmenu_run",  NULL };
static const char *termcmd[]  = { "st", NULL };

static const char volumeup[] = "pactl set-sink-volume @DEFAULT_SINK@ +5%; kill -39 $(pidof dwmblocks)";
static const char volumedown[] = "pactl set-sink-volume @DEFAULT_SINK@ -5%; kill -39 $(pidof dwmblocks)";
static const char volumemute[] = "pactl set-sink-mute @DEFAULT_SINK@ toggle; kill -39 $(pidof dwmblocks)";
static const char resetvolume[] = "pactl set-sink-volume @DEFAULT_SINK@ 100%; kill -39 $(pidof dwmblocks)";
static const char lightup[] = "brightnessctl s +10%; kill -44 $(pidof dwmblocks)";
static const char lightdown[] = "brightnessctl s 10%-; kill -44 $(pidof dwmblocks)";
static const char browsercmd[] = "brave";
static const char btopcmd[] = "st -e btop";
static const char mixercmd[] = "st -e pulsemixer";
static const char filebrowsercmd[] = "thunar";
static const char scrotcmd[] = "scrot -p -b";
static const char emojicmd[] = "emojiselector";
static const char killpicom[] = "pkill -9 picom; notify-send \" Picom Killed!\"";
static const char startpicom[] = "picom; notify-send \"Picom Started!\"";
static const char restartdunst[] = "pkill -9 dunst; dunst";

#include "movestack.c"
static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd} },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_n, spawn,          SHCMD(btopcmd) },
    { 0,                            XF86XK_AudioRaiseVolume, spawn,          SHCMD(volumeup) },
    { 0,                            XF86XK_AudioLowerVolume, spawn,          SHCMD(volumedown) },
    { 0,                            XF86XK_AudioMute, spawn,          SHCMD(volumemute) },
    { MODKEY,                       XF86XK_AudioRaiseVolume, spawn,          SHCMD(resetvolume) },
    { 0,                            XF86XK_MonBrightnessUp, spawn,          SHCMD(lightup) },
    { 0,                            XF86XK_MonBrightnessDown, spawn,          SHCMD(lightdown) },
    { ControlMask,                  XK_b, spawn,          SHCMD(browsercmd) },
    { MODKEY,                       XK_n, spawn,          SHCMD(filebrowsercmd) },
    { MODKEY,                       XK_s, spawn,          SHCMD(scrotcmd) },
    { MODKEY,                       XK_e, spawn,          SHCMD(emojicmd) },
    { MODKEY|ShiftMask,             XK_e, spawn,          SHCMD(mixercmd) },
    { MODKEY|ControlMask,           XK_p, spawn,          SHCMD(startpicom) },
    { MODKEY|ControlMask|ShiftMask, XK_p, spawn,          SHCMD(killpicom) },
    { MODKEY|ControlMask|ShiftMask, XK_d, spawn,          SHCMD(restartdunst) },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
